<?php
    // URL: https://wallflowergirldesigns.com/kinetic_dev_test/project-02.php

    class Connection 
    {
        private $servername = "localhost:3306";
		private $username   = "wallflq6_dev";
		private $password   = "wfg1873&#";
		private $database   = "wallflq6_dev-db";
		public  $con;


		// Database Connection 
		public function __construct()
		{
		    $this->con = new mysqli($this->servername, $this->username,$this->password,$this->database);

            if ($this->con->connect_error) {
                die("Connection failed: " . $this->con->connect_error);
              }
		}
    }

    class Products {
        public function displayProducts()
		{
            $db = new Connection();
		    $query = "SELECT * FROM products LEFT JOIN price_list ON products.product_id = price_list.product_id";
		    $result = $db->con->query($query);
            mysqli_close($db->con);

		    if ($result->num_rows > 0) {
		        $data = array();
		        while ($row = $result->fetch_assoc()) {
		            $data[] = $row;
		        }

			    return $data;
		    } else {
			    echo "No found records";
		    }
		}
    }

    class Cart
    {

        /**
         * Example of cart_items array structure
         * $cart_items = ['product_id' => ['quantity' => quantity, 'unit_price' => unit_price]]
         */

        public $cart_items = [];
        public $tax_percent = 10;
        private $price_list = [
            '1001' => 1299,
            '1002' => 927,
            '1003' => 1000,
            '1004' => 419,
            '1005' => 675,
        ];  

        public function displayCart() {
            $db = new Connection();
		    $query = "SELECT * FROM cart INNER JOIN products ON cart.product_id = products.product_id INNER JOIN price_list ON cart.product_id = price_list.product_id";
		    $result = $db->con->query($query);
            mysqli_close($db->con);

		    if ($result->num_rows > 0) {
		        //$data = array();
		        while ($row = $result->fetch_assoc()) {
		            $cart_items[] = $row;
		        }
			    return $cart_items;
                
		    } else {
			    echo "No found records";
		    }
		}

        public function addItemsToCart($post) {  
            $selected = $_POST['product'];
            $id = $_POST['product_name'];
            $quantity = $_POST['product_quantity'];
            $query_result = "";

            foreach($selected as $i)
            {
                $query_result .= "(". $id[$i] .",". $quantity[$i] ."),";    
               
            }
            $qr = rtrim($query_result, ",");

            $db = new Connection();
            $query = "DELETE FROM cart;";
            $query2 = "INSERT INTO cart(product_id, quantity) VALUES $qr;";
            $result = $db->con->query($query);
            $result2 = $db->con->query($query2);
         
           mysqli_close($db->con);
        }

        public function deleteItemsFromCart($post) {
            $selected = $_POST['cart'];
            $id = $_POST['cart_name'];
   
            $db = new Connection();

            foreach($selected as $i){
                $query_result = $id[$i]; 
                $query = "DELETE FROM cart WHERE product_id = $query_result;";
                $result = $db->con->query($query); 
            }

            mysqli_close($db->con);
        }

        public function updateItemsInCart($post) {
            $selected = $_POST['cart'];
            $id = $_POST['cart_name'];
            $quantity = $_POST['cart_quantity'];
   
            $db = new Connection();

            foreach($selected as $i){
                $query_result_id = $id[$i]; 
                $query_result_quantity = $quantity[$i];

                $query = "UPDATE cart SET quantity=$query_result_quantity WHERE product_id=$query_result_id";
                $result = $db->con->query($query); 
            }

            mysqli_close($db->con);
        }
    }

    $cartObj = new Cart();

    if(isset($_POST['add'])){
        if(!empty($_POST['product'])) {
            $cartObj->addItemsToCart($_POST);
        }
    }

    if (isset($_POST['delete'])) {
        if(!empty($_POST['cart'])) {
            $cartObj->deleteItemsFromCart($_POST);
        }
     }

     if (isset($_POST['update'])) {
        if(!empty($_POST['cart'])) {
            $cartObj->updateItemsInCart($_POST);
        }
     }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Kin PHP CRUD Operations</title>
    </head>
    <body>
        <form method="post">
            <table class="items" cellpadding="5" cellspacing="0">
                <tr>
                    <th class="checkbox">&nbsp;</th>
                    <th class="product">Item</th>
                    <th class="quantity">Quantity</th>
                    <th class="price">Price</th>
                </tr>
                <?php 
                    $productObj = new Products();
                    $products = $productObj->displayProducts(); 
                    $i = 0;
                    
                    foreach($products as $item) { ?>
                        <tr>
                            <td class="checkbox">
                                <input type="checkbox" id="<?php echo "product_" . $item['product_id'] ?>" onClick="check('<?php echo  $item['product_id'] ?>')" name="product[]" value="<?php echo $i ?>">
                            </td>
                            <td class="product">
                                <span><?php echo $item['name'] ?></span>
                                <input type="hidden" id="<?php echo "product_". $item['product_id']."_name" ?>" name="product_name[]" value="<?php echo $item['product_id'] ?>">
                            </td>    
                            <td class="quantity">
                                <input type="number" id="<?php echo "product_". $item['product_id']."_quantity" ?>" name="product_quantity[]" value="0"> 
                            </td>
                            <td class="price">
                                <span><?php echo '$'.  number_format(($item['price'] / 100) ,2,'.',',') ?></span>
                                <input type="hidden" id="<?php echo "product_". $item['product_id']."_price" ?>" name="product_price[]" value="<?php echo $item['price'] ?>"> 
                            </td>
                        </tr>
                        
                <?php $i++; } ?>
            </table>
             
            <button type="submit" name="add" class="add-items">Add to Cart</button>
        </form>

        <h2>Your Cart</h2>
        <?php
        $cart = $cartObj->displayCart();
        $tax = $cartObj->tax_percent;
        $total = 0;
        $ii = 0;

        if (empty($cart)) {
            echo 'Your cart is empty.';
        } else { ?>
            <form method="post">
                <table class="items" cellpadding="5" cellspacing="0">
                <tr>
                    <th class="checkbox">&nbsp;</th>
                    <th class="product">Item</th>
                    <th class="quantity">Quantity</th>
                    <th class="price">Price</th>
                </tr>
                <?php 
                    foreach($cart as $item) { ?>
                        <tr>
                            <td class="checkbox">
                                <input type="checkbox" id="<?php echo "cart_" . $item['product_id'] ?>" onClick="check('<?php echo  $item['product_id'] ?>')" name="cart[]" value="<?php echo $ii ?>">
                            </td>
                            <td class="product">
                                <span><?php echo $item['name'] ?></span>
                                <input type="hidden" id="<?php echo "cart_". $item['product_id']."_name" ?>" name="cart_name[]" value="<?php echo $item['product_id'] ?>">
                            </td>    
                            <td class="quantity">
                                <input type="number" id="<?php echo "cart_". $item['product_id']."_quantity" ?>" onchange="updateQuantity('<?php echo  $item['product_id'] ?>')" name="cart_quantity[]" value="<?php echo $item['quantity'] ?>"> 
                            </td>
                            <td class="price">
                                <span><?php echo '$'. round(($item['price'] / 100), 2) ?></span>
                                <input type="hidden" id="<?php echo "cart_". $item['product_id']."_price" ?>" name="cart_price[]" value="<?php echo $item['price'] ?>"> 
                            </td>
                        </tr> 
                     <?php 
                        $ii++; 
                        $total += ($item['quantity'] * $item['price']) + (($item['quantity'] * $item['price']) * ($tax/100));
            
                    } ?>
                        <tr>
                            <td colspan="3" style="text-align: right; font-weight: bold;">Total</td>
                            <td id="total"><?php echo '$'.  number_format(($total / 100) ,2,'.',',') ?></td>

                        </tr>
                </table>
                <button type="submit" name="update" class="update-items">Update Cart</button> &nbsp;&nbsp; <button type="submit" name="delete" class="delete-items">Remove Selected Items</button>
            </form>    
            <?php
        }?>

        <script type="text/javascript">
           function check(name) {
                var  item = document.getElementById('product_' + name);

                if (item.checked) {
                    document.getElementById('product_' + name + '_quantity').value = 1
                } else {
                    document.getElementById('product_' + name + '_quantity').value = 0
                }
            }

            function updateQuantity(name) {
                var quantity = document.getElementById('cart_' + name);
                quantity.checked = true;
            } 

        </script>
    </body>
</html>