<!DOCTYPE html>
<html>
<body>
    <ul>
        <?php 
            // URL: https://wallflowergirldesigns.com/kinetic_dev_test/project-01.php

            for($i = 1; $i <= 100; $i++) {
                
                echo ( '<li>' );
                switch ($i) {
                    case ( $i%15 == 0 ) :
                    echo ( 'FizzBuzz' );
                    break;

                    case( $i%3 == 0 ) : 
                    echo ( 'Fizz' );
                    break;

                    case ( $i%5 == 0 ) :
                    echo ( 'Buzz' );
                    break;

                    default:
                    echo ( $i );
                }

                echo ( '</li>' );
            }
        ?>
    </ul>
</body>
</html>